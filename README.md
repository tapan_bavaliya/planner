# planner

## Onboarding
* Code of Conduct -> https://nexus.improwised.com/w/guidelines/code-of-conduct/
* Company Policies -> https://nexus.improwised.com/w/playbook/
* Programming Environment -> https://nexus.improwised.com/w/kb/programming/environment/
* Software Design Best Practices https://nexus.improwised.com/w/kb/programming/software-design-best-practices/
* oh my zsh -> https://ohmyz.sh/

## Command-line
* Posix Command Line -> https://nexus.improwised.com/w/kb/programming/command-line/
* command Line -> http://www.linuxcommand.org/lc3_learning_the_shell.php

(if sucessful completed point 2)
* Command Line workshops -> https://www.udacity.com/course/linux-command-line-basics--ud595 -> start from here 9. The Terminal vs The Shell

Cheetsheets
* command cheet sheet -> https://files.fosswire.com/2007/08/fwunixref.pdf
* command cheet sheet -> http://cc.iiti.ac.in/docs/linuxcommands.pdf
* vi cheet sheet -> https://itsfoss.com/download-vi-cheat-sheet/ -> https://drive.google.com/file/d/0By49_3Av9sT1X3dlWkNQa3g2b2c/view

(only read in your free time)
* command cheet sheet -> https://docs.google.com/viewerng/viewer?url=https://www.loggly.com/wp-content/uploads/2015/05/Linux-Cheat-Sheet-Sponsored-By-Loggly.pdf&hl=en_US
* detailed cheet sheet -> http://cb.vu/unixtoolbox.pdf
* detailed cheet sheet -> https://perso.crans.org/~raffo/docs/linux-lpic-guide-3ed.pdf

## Git
* setup-> https://nexus.improwised.com/w/kb/programming/git/setup/
* git -> https://nexus.improwised.com/w/kb/programming/git/
* workshop -> https://nexus.improwised.com/w/kb/programming/git/setup/

## Javascript
* javascript -> https://nexus.improwised.com/w/kb/programming/javascript/
* course -> https://classroom.udacity.com/courses/ud803
* video -> https://www.youtube.com/watch?v=LTbnmiXWs2k&list=PL57atfCFqj2h5fpdZD-doGEIs0NZxeJTX
* class -> https://www.youtube.com/watch?v=T-HGdc8L-7w

## ES6 
`Note:` please don't read advance cover written in -> https://nexus.improwised.com/w/kb/programming/javascript/

* video -> https://www.youtube.com/watch?v=trvnP7EsAHA
* video -> https://www.youtube.com/watch?v=LTbnmiXWs2k&list=PL57atfCFqj2h5fpdZD-doGEIs0NZxeJTX
* blog -> https://codeburst.io/es6-tutorial-for-beginners-5f3c4e7960be
* blog -> https://blog.jscrambler.com/8-awesome-es6-features/

## Funtional js
* Introduction funtional js -> https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/functional-programming/
* blog -> https://opensource.com/article/17/6/functional-javascript

## Async exercise + promise explanation
* Async -> https://javascript.info/async

## Node
`NOTE: ` following points you need to cover at least -> (File system (read ,read file sync, read dir), Url (class:url, url format), Os(cpus, hostname, homer, platform), Child process(exec), Console(count, time, log, warn, error, timeend), path(delimiter, dirname, extname, format, join, normalize, relative, resolve), Query string)

* https://github.com/workshopper/learnyounode
* basic of node js -> https://www.airpair.com/javascript/node-js-tutorial
* Node.js Tutorial -> https://www.tutorialspoint.com/nodejs/index.htm
* basic funtion -> https://www.c-sharpcorner.com/article/nodejs-getting-started-with-some-basic-functions/
* api(official doc) -> https://nodejs.org/api/

## Booststrap
* introduction - https://getbootstrap.com/docs/4.4/getting-started/introduction/
* download - https://getbootstrap.com/docs/4.4/getting-started/download/
* layout & all sub topics - https://getbootstrap.com/docs/4.4/layout/overview/
* content & all sub topics - https://getbootstrap.com/docs/4.4/content/reboot/
* components - https://getbootstrap.com/docs/4.4/components/alerts/ - go throught all the default component
* utilities - https://getbootstrap.com/docs/4.4/utilities/borders/